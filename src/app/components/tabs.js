'use strict';
let generateMaterialInputs = require('./../form/text-field').generateMaterialInputs;
let aboutTabScript = require('./../about/about');
let updateProfile = require('./../profile/profile').updateProfile;
let attachPopupEvents = require('./../components/popup-edit').attachPopupEvents;

// Vertical touch scrolling
const tabsLinks = document.querySelector('.tabs__links');

tabsLinks.addEventListener('touchmove', tabDragStart);
tabsLinks.addEventListener('touchend', tabDragStop);

let scrollDragTouch = 0;
function tabDragStart(e) {
  const touch = e.changedTouches[0]; // First touch point (first finger)
  const amountToScroll = scrollDragTouch - parseInt(touch.clientX);console.log(touch.clientX)
  if (scrollDragTouch !== 0) {
    if (amountToScroll < 0) {
      tabsLinks.scrollLeft += amountToScroll;
    } else if (amountToScroll > 0) {
      tabsLinks.scrollLeft += amountToScroll;
    }
  }
  scrollDragTouch = parseInt(touch.clientX);
}

function tabDragStop() {
  scrollDragTouch = 0;
}

// Tab link event
const firstTabLink = document.querySelector('.tabs__link');
const tabsLinksList = document.querySelectorAll('.tabs__link');
const tabsContainer = document.querySelector('.tabs__container');
const tabActiveClass = 'tabs__link--active';

for (let i = 0; i < tabsLinksList.length; i++) {
  let tabLink = tabsLinksList[i];
  tabLink.addEventListener('click', (e) => {
    const activeTab = document.querySelector(`.${tabActiveClass}`);
    activeTab && activeTab.classList.remove(tabActiveClass);
    tabLink.classList.add(tabActiveClass);
    tabsContainer.innerHTML = '<div class="tabs__loading">Loading tab...</div>';
    loadTab(tabLink.getAttribute('href'), (res) => {
      tabsContainer.innerHTML = res;
      generateMaterialInputs(tabsContainer.querySelectorAll('.text-field__input'));
      attachPopupEvents(tabsContainer.querySelectorAll('.popup-edit__button'))
      if (tabLink.classList.contains('about-tab')) {
        aboutTabScript();
        updateProfile();
      }
    }, () => {
      tabsContainer.innerHTML = '<div class="tabs__error">Tab error, try again.</div>';
    });
    e.preventDefault();
  });
}

// Load the first one
firstTabLink.click();

// Tabs content dynamic loading
function loadTab(url, success, error) {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url);
  xhr.onreadystatechange = () => {
    if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
      success && success(xhr.responseText);
    } else {
      error && error();
    }
  };
  xhr.send();
}


