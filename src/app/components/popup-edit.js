'use strict';

let generateMaterialInputs = require('./../form/text-field').generateMaterialInputs;
let updateProfile = require('./../profile/profile').updateProfile;

module.exports = {
  attachPopupEvents
};

function attachPopupEvents(buttons) {
  for (let i = 0; i < buttons.length; i++) {
    let button = buttons[i];
    let inputFields = button.getAttribute('data-popup-edit-field').split(',');
    let inputClasses = button.getAttribute('data-popup-edit-live').split(',');
    let inputLabels = button.getAttribute('data-popup-edit-label').split(',');
    let inputMaxlength = button.getAttribute('data-popup-edit-max').split(',');
    let inputs = '';
    for (let j = 0; j < inputFields.length; j++) {
      inputs += `
        <div class="text-field text-field--not-empty text-field--floating">
          <label class="text-field__label text-field__label--floating">${inputLabels[j]}</label>
          <input class="text-field__input"
                 data-live="${inputClasses[j]}"
                 maxlength="${inputMaxlength[j]}"
                 required>
        </div>
      `;
    }
    let popup = document.createElement('div');
    popup.classList.add('popup');
    popup.innerHTML = `
      <form>
        ${inputs}
        <div class="popup__actions">
          <button class="button button--fill about__save-button">Save</button>
          <button type="button" class="button about__cancel-button">Cancel</button>
        </div>
      </form>
    `;

    generateMaterialInputs(popup.querySelectorAll('input'));

    button.addEventListener('click', () => {
      let prevPopup = document.querySelector('.popup');
      prevPopup && prevPopup.parentNode.removeChild(prevPopup);
      button.parentNode.querySelector('.popup-edit__info').appendChild(popup);

      let profile = JSON.parse(localStorage.profile);
      for (let j = 0; j < inputFields.length; j++) {
        popup.querySelectorAll('input')[j].value = profile[inputFields[j]];
      }

      popup.querySelector('input').focus();
    });

    popup.querySelector('.about__cancel-button').addEventListener('click', function () {
      let profile = JSON.parse(localStorage.profile);

      for (let j = 0; j < inputFields.length; j++) {
        popup.querySelectorAll('input')[j].value = profile[inputFields[j]];
      }

      updateProfile();

      popup.parentNode.removeChild(popup);
    });

    popup.querySelector('.about__save-button').addEventListener('click', function (e) {
      if (!popup.querySelector('form').checkValidity()) {
        return;
      }

      let profile = JSON.parse(localStorage.profile);

      for (let j = 0; j < inputFields.length; j++) {
        profile[inputFields[j]] = popup.querySelectorAll('input')[j].value;
      }

      localStorage.profile = JSON.stringify(profile);

      popup.parentNode.removeChild(popup);

      e.preventDefault();
    });

  }

}

