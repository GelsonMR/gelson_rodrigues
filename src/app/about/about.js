'use strict';

let updateProfile = require('./../profile/profile').updateProfile;

module.exports = about;

function about() {
  const aboutTab = document.querySelector('.tab.about');

  document.querySelector('.about__edit-button').addEventListener('click', () => {
    aboutTab.classList.add('about--edit-mode-on');
    aboutTab.classList.remove('about--edit-mode-off');

    let profile = JSON.parse(localStorage.profile);
    document.querySelector('#txtFirstName').value = profile.firstName;
    document.querySelector('#txtLastName').value = profile.lastName;
    document.querySelector('#txtWebsite').value = profile.website;
    document.querySelector('#txtPhone').value = profile.phone;
    document.querySelector('#txtAddress').value = profile.address;

    const inputs = document.querySelectorAll('input');
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].blur();
    }
  });

  document.querySelector('.about__cancel-button').addEventListener('click', () => {
    aboutTab.classList.add('about--edit-mode-off');
    aboutTab.classList.remove('about--edit-mode-on');

    updateProfile();
  });

  document.querySelector('.about__save-button').addEventListener('click', (e) => {
    if (!formAbout.checkValidity()) {
      return;
    }

    aboutTab.classList.add('about--edit-mode-off');
    aboutTab.classList.remove('about--edit-mode-on');

    let profile = JSON.parse(localStorage.profile);

    profile.firstName = document.querySelector('#txtFirstName').value;
    profile.lastName = document.querySelector('#txtLastName').value;
    profile.website = document.querySelector('#txtWebsite').value;
    profile.phone = document.querySelector('#txtPhone').value;
    profile.address = document.querySelector('#txtAddress').value;

    updateProfile(profile);

    e.preventDefault();
  });
}
