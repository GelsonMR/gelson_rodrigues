'use strict';

module.exports = {
  updateProfile
};

// Default values for profile
if (!localStorage.profile) {
  localStorage.profile = JSON.stringify({
    "firstName": "Jessica",
    "lastName": "Parker",
    "website": "www.seller.com",
    "phone": "(949) 325 - 68594",
    "address": "Newport Beach CA",
    "stars": 0,
    "reviews": 5,
    "reviewed": false,
    "followers": 15,
    "following": false
  });
}

// Update values
updateProfile();

function updateProfile(profile) {
  profile = profile || JSON.parse(localStorage.profile);

  updateProfileValues(document.querySelectorAll('.profile__first-name'), profile.firstName);
  updateProfileValues(document.querySelectorAll('.profile__last-name'), profile.lastName);
  updateProfileValues(document.querySelectorAll('.profile--phone .value'), profile.phone);
  updateProfileValues(document.querySelectorAll('.profile--website .value'), profile.website);
  updateProfileValues(document.querySelectorAll('.profile--address .value'), profile.address);
  updateProfileValues(document.querySelectorAll('.profile__total-reviews'), profile.reviews);
  updateProfileValues(document.querySelectorAll('.profile__total-followers'), profile.followers);

  if (profile.reviewed) {
    document.querySelector('.profile__review').classList.add('profile__review--rated');
    document.querySelector('.profile__review').classList.remove('profile__review--not-rated');
    document.querySelectorAll('.profile__star')[profile.stars - 1].classList.add('profile__star--active');
  } else {
    document.querySelector('.profile__review').classList.add('profile__review--not-rated');
    document.querySelector('.profile__review').classList.remove('profile__review--rated');
  }

  if (profile.following) {
    document.querySelector('.profile__icon--follow').classList.add('profile__icon--following');
  } else {
    document.querySelector('.profile__icon--follow').classList.remove('profile__icon--following');
  }

  // In case there are new values, the storage is updated
  localStorage.profile = JSON.stringify(profile);
}

function updateProfileValues(elements, value) {
  for (let i = 0; i < elements.length; i++) {
    elements[i].innerHTML = value;
  }
}

// Reviews
const stars = document.querySelectorAll('.profile__star');
const activeClass = 'profile__star--active';
const profileReviewClassList = document.querySelector('.profile__review').classList;
const totalReviews = document.querySelector('.profile__total-reviews');

for (let i = 0; i < stars.length; i++) {
  let star = stars[i];
  let starClick = () => {
    let activeStar = document.querySelector(`.${activeClass}`);
    let profile = JSON.parse(localStorage.profile);

    if (star.classList.contains(activeClass)) {
      activeStar && activeStar.classList.remove(activeClass);
      profileReviewClassList.add('profile__review--not-rated');
      profileReviewClassList.remove('profile__review--rated');
      profile.reviews--;
      profile.reviewed = false;
      profile.stars = 0;
    } else {
      if (!profile.reviewed) {
        profile.reviews++;
        profile.reviewed = true;
      }
      profile.stars = i + 1;
      profileReviewClassList.add('profile__review--rated');
      profileReviewClassList.remove('profile__review--not-rated');
      activeStar && activeStar.classList.remove(activeClass);
      star.classList.add(activeClass);
    }

    totalReviews.innerHTML = profile.reviews;
    localStorage.profile = JSON.stringify(profile);
  };
  star.addEventListener('click', starClick);
}

// Reviews
const followIcon = document.querySelector('.profile__icon--follow');
const followingClass = 'profile__icon--following';
const totalFollowers = document.querySelector('.profile__total-followers');

let followClick = () => {
  let profile = JSON.parse(localStorage.profile);
  if (profile.following) {
    profile.followers--;
    profile.following = false;
    totalFollowers.innerHTML = profile.followers;
    followIcon.classList.remove(followingClass);
  } else {
    profile.followers++;
    profile.following = true;
    totalFollowers.innerHTML = profile.followers;
    followIcon.classList.add(followingClass);
  }
  localStorage.profile = JSON.stringify(profile);
};
followIcon.addEventListener('click', followClick);
