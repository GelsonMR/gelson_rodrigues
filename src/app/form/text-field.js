'use strict';

module.exports = {
  generateMaterialInputs
};

const textInputs = document.querySelectorAll('.text-field__input');
generateMaterialInputs(textInputs);

function generateMaterialInputs(textInputs) {
  for (let i = 0; i < textInputs.length; i++) {
    let textInput = textInputs[i];
    let line = document.createElement('div');
    line.classList.add('text-field__line');
    textInput.parentNode.insertBefore(line, textInput.nextSibling);

    textInput.addEventListener('focus', () => {
      textInput.parentNode.classList.add('text-field--focus');
    });
    textInput.addEventListener('blur', () => {
      textInput.parentNode.classList.remove('text-field--focus');
      if (textInput.value) {
        textInput.parentNode.classList.add('text-field--not-empty');
      } else {
        textInput.parentNode.classList.remove('text-field--not-empty');
      }
    });
    textInput.addEventListener('input', () => {
      if (textInput.checkValidity()) {
        textInput.parentNode.classList.remove('text-field--invalid');
      } else {
        textInput.parentNode.classList.add('text-field--invalid');
      }
      if (textInput.value) {
        textInput.parentNode.classList.add('text-field--not-empty');
      } else {
        textInput.parentNode.classList.remove('text-field--not-empty');
      }
      if (textInput.hasAttribute('data-live')) {
        const liveFields = document.querySelectorAll(textInput.getAttribute('data-live'));
        for (let i = 0; i < liveFields.length; i++) {
          liveFields[i].innerHTML = textInput.value;
        }
      }
    });
  }
}
